import { useContext, useEffect, useState } from 'react'
import authContext from './AuthContext'
import jwtService from '../services/authService/jwtService'

export function useAuth() {
  return useContext(authContext)
}

export function useProviderAuth() {
  const [user, setUser] = useState(null)

  const handleSignUp = async ({ email, password, name }, cb) => {
    const res = await jwtService.signUp({ email, password, name })
    setUser(res)

    if (typeof cb === 'function') cb(res)
  }

  const handleLogin = async (email, password, cb) => {
    const res = await jwtService.signInWithEmailAndPassword(email, password)

    setUser(res)

    if (typeof cb === 'function') cb(res)
  }

  const handleLogout = (cb) => {
    console.log('LOGOUT')
    jwtService.logout()
    setUser(null)
    if (typeof cb === 'function') cb()
  }

  useEffect(() => {
    const res = jwtService.init()
    if (res) {
      jwtService.signInWithToken(res).then((user) => {
        if (user) setUser(user)
        else setUser(null)
      })
    }
  }, [])

  return {
    user,
    handleLogin,
    handleLogout,
    handleSignUp,
  }
}
