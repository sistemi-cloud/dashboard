#!/bin/sh

if [ -f ./package.json ]; then
  grep version < ./package.json | head -1 | awk -F: '{ print $2 }' | sed 's/[",]//g' | sed 's/ //g'
else
  echo "${0} ERROR: package.json file not found"
  exit 1
fi
