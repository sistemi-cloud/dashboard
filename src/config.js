let env = 'development'
if (process.env.NODE_ENV === 'production' && !process.env.REACT_APP_STAGING) {
  env = 'production'
}

const configuration = {
  all: {
    env,
  },
  test: {},
  development: {
    apiUrl: 'https://backend-project.staging.cloudservices-ct.it',
    /*wsUrl: 'wss://websocket.staging.parksmart.it',
    googleMapsApiKey: 'AIzaSyDb_22VSE0vMLC2zFRxg793cz7uFoxnEZg',
    aws: {
      mqttHost: 'a3rzums6u8jbaw-ats.iot.eu-west-1.amazonaws.com',
      region: 'eu-west-1',
      cognitoIdentityPool: 'eu-west-1:cf657963-74c0-45be-84cb-fb5adc6a9433'
    }*/
  },
  production: {
    apiUrl: 'https://backend-project.cloudservices-ct.it',
    /*wsUrl: 'wss://websocket.parksmart.it',
    googleMapsApiKey: 'AIzaSyDb_22VSE0vMLC2zFRxg793cz7uFoxnEZg',
    aws: {
      mqttHost: 'a3rzums6u8jbaw-ats.iot.eu-west-1.amazonaws.com',
      region: 'eu-west-1',
      cognitoIdentityPool: 'eu-west-1:cf657963-74c0-45be-84cb-fb5adc6a9433'
    }*/
  },
}
export const config = { ...configuration.all, ...configuration[configuration.all.env] }
export default config
