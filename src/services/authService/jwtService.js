import jwtDecode from 'jwt-decode'

import API from '../'
/* eslint-disable camelcase */

class JwtService {
  init() {
    // this.setInterceptors();
    return this.handleAuthentication()
  }

  /*setInterceptors = () => {
    const token = this.getAccessToken();
    API.interceptors.response.use(
      response => {
        return response;
      },
      err => {
        return new Promise((resolve, reject) => {
          console.log(err.response);
          console.log(err.config);
          console.log(!err.config.__isRetryRequest);
          console.log(token);
          console.log(!token);
          console.log(
            err.response &&
            err.response.status === 401 &&
            err.config &&
            !err.config.__isRetryRequest &&
            !token
          );
          console.log(
            err.response &&
            err.response.status === 401 &&
            err.config &&
            !err.config.__isRetryRequest &&
            token
          );
          if (
            err.response &&
            err.response.status === 404 &&
            err.config &&
            !err.config.__isRetryRequest &&
            token
          ) {
          } else if (
            err.response &&
            err.response.status === 401 &&
            err.config &&
            !err.config.__isRetryRequest &&
            !token
          ) {
            // if you ever get an unauthorized response, logout the user
            this.emit('onAutoLogout', 'Invalid access_token');
            this.setSession(null);
          } else if (
            err.response &&
            err.response.status === 401 &&
            err.config &&
            !err.config.__isRetryRequest &&
            token
          ) {
            // not authorized
            this.emit('onAutoLogout', 'Session expired');
          } else if (
            err.response &&
            err.response.status === 401 &&
            err.config &&
            !err.config.__isRetryRequest &&
            !token
          ) {
            this.emit('onAutoLogout', 'Invalid username or password.');
          } else if (
            err.response &&
            err.response.status === 423 &&
            err.config &&
            !err.config.__isRetryRequest &&
            !token
          ) {
            this.emit('onAutoLogout', 'Account confirmation required!');
          }
          throw err;
        });
      }
    );
  };*/

  handleAuthentication = () => {
    const access_token = this.getAccessToken()

    if (!access_token) {
      // this.emit('onNoAccessToken');
      return false
    }

    if (this.isAuthTokenValid(access_token)) {
      this.setSession(access_token)
      return access_token
      // this.emit('onAutoLogin', true);
    } else {
      this.setSession(null)
      // this.emit('onAutoLogout', 'access_token expired');
    }
    return false
  }

  createUser = (data) => {
    return new Promise((resolve, reject) => {
      API.post('/users', data)
        .then((response) => {
          if (response.data) {
            // this.setSession(response.data.access_token);
            resolve(response.data)
          } else {
            reject(response.data.error)
          }
        })
        .catch((error) => reject(error))
    })
  }

  signInWithEmailAndPassword = (email, password) => {
    return new Promise((resolve, reject) => {
      API.post('/auth/login', {
        email,
        password,
      }).then((response) => {
        if (response.data.user) {
          this.setSession(response.data.token)
          // this.emit('onAutoLogin', true);
          resolve(response.data.user)
        } else {
          reject(response.data.error)
        }
      })
    })
  }

  signInWithToken = () => {
    return new Promise((resolve, reject) => {
      API.get('/users/me', {
        data: {
          access_token: this.getAccessToken(),
        },
      })
        .then((response) => {
          if (response.data) {
            // this.setSession(response.data.access_token);
            // this.emit('onAutoLogin', true);
            resolve(response.data)
          } else {
            this.logout()
            reject(new Error('Failed to login with token.'))
          }
        })
        .catch((error) => {
          this.logout()
          reject(new Error('Failed to login with token.'))
        })
    })
  }

  signUp = (user) => {
    return new Promise((resolve, reject) => {
      API.post('/auth/register', {
        ...user,
      })
        .then((res) => {
          if (res.status === 201 && res.data.user) {
            this.setSession(res.data.token)
            // this.emit('onAutoLogin', true);
            resolve(res.data.user)
          } else {
            reject(res.data.error)
          }
        })
        .catch((error) => reject(error))
    })
  }

  updateUserData = (user) => {
    return new Promise((resolve, reject) => {
      API.put(
        `/users/${user.id}`,
        {
          ...user,
        },
        { access_token: this.getAccessToken() },
      )
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => reject(error))
    })
  }

  deleteUserData = (id) => {
    return new Promise((resolve, reject) => {
      API.delete(`/users/${id}`, { access_token: this.getAccessToken() })
        .then((response) => {
          resolve(response.data)
        })
        .catch((error) => reject(error))
    })
  }

  listUsers = (params) => {
    return new Promise((resolve, reject) => {
      API.get('/users', {
        data: {
          access_token: this.getAccessToken(),
        },
        params,
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data)
          }
        })
        .catch((error) => reject(error))
    })
  }

  getUser = (idUser) => {
    return new Promise((resolve, reject) => {
      API.get(`/users/${idUser}`, {
        data: {
          access_token: this.getAccessToken(),
        },
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data)
          }
        })
        .catch((error) => reject(error))
    })
  }

  getScopes = () => {
    return new Promise((resolve, reject) => {
      API.get('/users/scopes', {
        data: {
          access_token: this.getAccessToken(),
        },
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data)
          }
        })
        .catch((error) => reject(error))
    })
  }

  /*getCognitoCreds = () => {
    return new Promise((resolve, reject) => {
      axios
        .get('/auth/cognito', {
          data: {
            access_token: this.getAccessToken()
          }
        })
        .then(response => {
          if (response.data) {
            resolve(response.data);
          }
        })
        .catch(error => reject(error));
    });
  };*/

  setSession = (access_token) => {
    if (access_token) {
      localStorage.setItem('jwt_access_token', access_token)
      API.defaults.headers.common.Authorization = `Bearer ${access_token}`
    } else {
      localStorage.removeItem('jwt_access_token')
      delete API.defaults.headers.common.Authorization
    }
  }

  logout = () => {
    return new Promise((resolve, reject) => {
      API.post('/auth/logout', {
        data: {
          access_token: this.getAccessToken(),
        },
      })
        .then((response) => {
          if (response.status === 200) this.setSession(null)
          resolve(response)
        })
        .catch((error) => reject(error))
    })
  }

  isAuthTokenValid = (access_token) => {
    if (!access_token) {
      return false
    }
    const decoded = jwtDecode(access_token)
    const currentTime = Date.now() / 1000
    if (decoded.exp && decoded.exp < currentTime) {
      console.warn('access token expired')
      return false
    }

    return true
  }

  getAccessToken = () => {
    return window.localStorage.getItem('jwt_access_token')
  }
}

const instance = new JwtService()

export default instance
