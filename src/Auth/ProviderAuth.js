import React from 'react'
import PropTypes from 'prop-types'
import authContext from './AuthContext'

import { useProviderAuth } from './AuthHooks'
const ProviderAuth = ({ children }) => {
  const auth = useProviderAuth()

  return <authContext.Provider value={auth}>{children}</authContext.Provider>
}

ProviderAuth.propTypes = {
  children: PropTypes.object,
}
export default ProviderAuth
