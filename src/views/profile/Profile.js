import React, { useContext } from 'react'

import {
  CAvatar,
  CCard,
  CCardBody,
  CCardHeader,
  CCardText,
  CCardTitle,
  CCol,
  CRow,
} from '@coreui/react'

import authContext from '../../Auth/AuthContext'
import { useNavigate, useLocation } from 'react-router-dom'

const Profile = () => {
  const auth = useContext(authContext)
  return (
    <>
      <CCard>
        <CCardHeader>
          <CRow className="justify-content-center">
            <CAvatar src={auth.user.picture} size="xl" />
          </CRow>
        </CCardHeader>
        <CCardBody>
          <CCol className="justify-content-center mb-5">
            <CCardTitle>Full name</CCardTitle>
            <CCardText className="small text-medium-emphasis">{auth.user.name}</CCardText>
          </CCol>
          <CCol className="justify-content-center mb-5">
            <CCardTitle>Email</CCardTitle>
            <CCardText className="small text-medium-emphasis">{auth.user.email}</CCardText>
          </CCol>
          <CCol className="justify-content-center">
            <CCardTitle>Role</CCardTitle>
            <CCardText className="small text-medium-emphasis">{auth.user.role}</CCardText>
          </CCol>
        </CCardBody>
      </CCard>
    </>
  )
}
export default Profile
