import React from 'react'
import { useAuth } from './AuthHooks'
import { Navigate, Outlet } from 'react-router-dom'

const PrivateRoute = () => {
  const auth = useAuth()

  return auth.user ? (
    <Outlet />
  ) : (
    <Navigate
      to={{
        pathname: '/login',
        // state: { from: location },
      }}
    />
  )

  /*return (
    <Route
      {...rest}
      render={({ location }) =>
        auth.user ? (
          children
        ) : (
          <Navigate
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  )*/
}
export default PrivateRoute
