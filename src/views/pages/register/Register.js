import React, { useContext, useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { cilLockLocked, cilUser } from '@coreui/icons'
import authContext from '../../../Auth/AuthContext'
import { useLocation, useNavigate } from 'react-router-dom'

const Register = () => {
  const auth = useContext(authContext)
  const location = useLocation()
  const navigate = useNavigate()

  const { from } = location.state || { from: { pathname: '/' } }

  const [formData, setFormData] = useState({
    password: '',
    confirmPassword: false,
    name: '',
    email: '',
  })

  const handleFormChange = (field, val) => {
    setFormData((prev) => ({
      ...prev,
      [field]: val,
    }))
  }

  return (
    <div className="bg-light min-vh-100 d-flex flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6}>
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm>
                  <h1>Register</h1>
                  <p className="text-medium-emphasis">Create your account</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilUser} />
                    </CInputGroupText>
                    <CFormInput
                      onChange={({
                        nativeEvent: {
                          target: { value },
                        },
                      }) => {
                        handleFormChange('name', value)
                      }}
                      name="name"
                      placeholder="Full name"
                      autoComplete="name"
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>@</CInputGroupText>
                    <CFormInput
                      name="email"
                      placeholder="Email"
                      autoComplete="email"
                      onChange={({
                        nativeEvent: {
                          target: { value },
                        },
                      }) => {
                        handleFormChange('email', value)
                      }}
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      name="password"
                      type="password"
                      placeholder="Password"
                      autoComplete="password"
                      onChange={({
                        nativeEvent: {
                          target: { value },
                        },
                      }) => {
                        handleFormChange('password', value)
                      }}
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-4">
                    <CInputGroupText>
                      <CIcon icon={cilLockLocked} />
                    </CInputGroupText>
                    <CFormInput
                      name="confirmPassword"
                      type="password"
                      placeholder="Repeat password"
                      autoComplete="password"
                      onChange={({
                        nativeEvent: {
                          target: { value },
                        },
                      }) => {
                        if (value === formData.password) handleFormChange('confirmPassword', true)
                        else handleFormChange('confirmPassword', false)
                      }}
                    />
                  </CInputGroup>
                  <div className="d-grid">
                    <CButton
                      onClick={() =>
                        auth.handleSignUp(formData, (res) => {
                          navigate(from, { replace: true })
                        })
                      }
                      color="success"
                    >
                      Create Account
                    </CButton>
                  </div>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Register
