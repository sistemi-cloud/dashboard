FROM node:16 AS builder

RUN mkdir -p /usr/share/nginx/app

COPY package.json /usr/share/nginx/app
COPY package-lock.json /usr/share/nginx/app
COPY .nvmrc /usr/share/nginx/app

COPY .eslintrc.js /usr/share/nginx/app
COPY .prettierrc.js /usr/share/nginx/app
COPY .prettierignore /usr/share/nginx/app
COPY jsconfig.json /usr/share/nginx/app

WORKDIR /usr/share/nginx/app

RUN npm i;
COPY public /usr/share/nginx/app/public
COPY src /usr/share/nginx/app/src
RUN ls
RUN ls public
RUN ls src
RUN npm run build

FROM nginx
RUN mkdir -p /usr/share/nginx/app
COPY --from=builder /usr/share/nginx/app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
RUN chown -R nginx:nginx /usr/share/nginx/html
